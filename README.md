# nuxt-netlify

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Deploy
* Get `NETLIFY_AUTH_TOKEN`
  * Access to following page and Click `Personal Access Token`.
  * https://app.netlify.com/user/applications#personal-access-tokens

* Get `NETLIFY_SITE_ID`
  * Access to `Site Details`(`Your Site Page` > `Site Settings` > `Site Details` > `Site Information`)
  * Get the value of `API ID`.
